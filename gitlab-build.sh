#!/bin/bash

pacman -Sy
pacman -S --noconfirm wget curl git base-devel extra-cmake-modules plasma-meta sshpass

git clone https://aur.archlinux.org/yay.git
chmod -R 777 yay
cd yay
sudo -u bruno script -q -c "/usr/bin/makepkg -si --noconfirm --noprogressbar" /dev/null 
sudo -u bruno script -q -c "/usr/bin/yay -S plymouth --noconfirm" /dev/null  
cd ..

cd PKGBUILD
sudo -u bruno script -q -c "/usr/bin/makepkg --noconfirm --noprogressbar --sign -Csfc" /dev/null 

source PKGBUILD

export SSHPASS=$DEPLOY_PASS

sshpass -e scp -q -o stricthostkeychecking=no $pkgname-$pkgver-$pkgrel-$arch.pkg.tar.xz $DEPLOY_USER@$DEPLOY_HOST:$DEPLOY_PATH
sshpass -e ssh $DEPLOY_USER@$DEPLOY_HOST 'bash /home/packager/repositories/nomad-desktop/repositories_util.sh'


#source PKGBUILD/PKGBUILD

#pacman -Sy
#pacman -S --noconfirm wget curl git base-devel extra-cmake-modules plasma-meta sshpass

#git clone https://aur.archlinux.org/yay.git
#chmod -R 777 yay
#cd yay
#sudo -u bruno script -q -c "/usr/bin/makepkg -si --noconfirm --noprogressbar" /dev/null 
#sudo -u bruno script -q -c "/usr/bin/yay -S plymouth --noconfirm" /dev/null  
#cd ..
#
#cd PKGBUILD
#sudo -u bruno script -q -c "/usr/bin/makepkg --noconfirm --noprogressbar --sign -Csfc" /dev/null 
#
#export SSHPASS=$DEPLOY_PASS
#
#sshpass -e scp -q -o stricthostkeychecking=no $pkgname-$pkgver-$pkgrel-$arch.pkg.tar.xz $DEPLOY_USER@$DEPLOY_HOST:$DEPLOY_PATH